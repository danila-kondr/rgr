.POSIX:
.SUFFIXES:

CC=x86_64-w64-mingw32-gcc
LIBS=-lm -luser32 -lkernel32 -lgdi32
OBJS=main.o tabfn.o cls.o author.o menu.o equation.o integral.o \
     gdimagic.o root.o
CFLAGS=

rgr.exe: $(OBJS)
	$(CC) -o rgr.exe $(OBJS) $(LIBS)

.SUFFIXES: .c .o
.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f *.o rgr.exe
