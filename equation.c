/**
 * Rozchotno-graficheskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * equation.c
 *
 * Reshenije uravnenija dvumia metodami: metodom bisekciji i metodom
 * xord.
 *
 * Uravnenije:
 *     2*x*lg(x) - 3 = 0 (variant 4)
 */
#include "rgr.h"
#include <math.h>

#define eps 0.001
#define a   eps
#define b   10.0

static double fn(double x)
{
	return 2 * x * log10(x) - 3;
}

static void equation_bisection(HANDLE hConsole, HWND hConsoleWindow)
{
	double l, r, m;
	double mul;

	puts("Решение уравнения методом бисекции:");
	puts("         2*x*lg(x) - 3 = 0");
	printf("на отрезке [%lg; %lg], eps=%lg\n", a, b, eps);

	l = a;
	r = b;
	do {
		m = (l + r) / 2;
		if (fabs(fn(m)) > eps) {
			mul = fn(l) * fn(m);
			if (mul < 0) {
				r = m;
			} else {
				l = m;
			}
		}
	} while (fabs(fn(m)) > eps);

	printf("x=%lg\n", m);
}

static void equation_chords(HANDLE hConsole, HWND hConsoleWindow)
{
	double l, r, m;
	double mul;

	puts("Решение уравнения методом хорд:");
	puts("         2*x*lg(x) - 3 = 0");
	printf("на отрезке [%lg; %lg], eps=%lg\n", a, b, eps);

	l = a;
	r = b;
	do {
		m = (l*fn(r) - r*fn(l)) / (fn(r) - fn(l));
		if (fabs(fn(m)) > eps) {
			mul = fn(l) * fn(m);
			if (mul < 0) {
				r = m;
			} else {
				l = m;
			}
		}
	} while (fabs(fn(m)) > eps);

	printf("x=%lg\n", m);
}

void equation(HANDLE hConsole, HWND hConsoleWindow)
{
	equation_bisection(hConsole, hConsoleWindow);
	printf("\n");
	equation_chords(hConsole, hConsoleWindow);

	_getch();
}
