/**
 * Rozchotno-graficheskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * main.c
 *
 * Glavnyj fajl. Tochka vxoda vsego prilozhenija.
 * Zdesj v cikle krutitsia meniu.
 */
#include "rgr.h"

static
void placeholder(HANDLE, HWND);

int main(void)
{
	HANDLE hConsole;
	HWND hConsoleWindow;
	CONSOLE_CURSOR_INFO cursor_info, hidden_cursor_info;
	int menu_point = 0;

#if _WIN32_WINNT >= 0x0600
	SetProcessDPIAware();
#endif
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	hConsoleWindow = GetConsoleWindow();
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	
	GetConsoleCursorInfo(hConsole, &cursor_info);

	hidden_cursor_info = cursor_info;
	hidden_cursor_info.bVisible = FALSE;

	cls(hConsole);

	while (TRUE) {
		menu_point = menu(hConsole, hConsoleWindow);

		cls(hConsole);
		switch (menu_point) {
		case MENU_ITEM_TABLE:
			tabulation(hConsole, hConsoleWindow);
			break;
		case MENU_ITEM_GRAPH:
			SetConsoleCursorInfo(hConsole, &hidden_cursor_info);
			graph(hConsole, hConsoleWindow);
			break;
		case MENU_ITEM_EQUATION:
			equation(hConsole, hConsoleWindow);
			break;
		case MENU_ITEM_INTEGRAL:
			integral(hConsole, hConsoleWindow);
			break;
		case MENU_ITEM_SPLASH:
			SetConsoleCursorInfo(hConsole, &hidden_cursor_info);
			splash(hConsole, hConsoleWindow);
			break;
		case MENU_ITEM_INFO:
			SetConsoleCursorInfo(hConsole, &hidden_cursor_info);
			author_info(hConsole, hConsoleWindow);
			break;
		case N_MENU_ITEMS:
			goto end;
		default:
			placeholder(hConsole, hConsoleWindow);
			break;
		}
		clw(hConsoleWindow);
		cls(hConsole);
		SetConsoleCursorInfo(hConsole, &cursor_info);
	}

 end:	return 0;
}

static
void placeholder(HANDLE hConsole, HWND hConsoleWindow)
{
	puts("Этот пункт меню ещё не реализован.");
	puts("Пожалуйста, нажмите любую кнопку для продолжения...");
	_getch();
}
