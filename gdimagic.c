/**
 * Rozchotno-graficheskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * gdimagic.c
 *
 * Magija GDI. Graficheskaja zastavka.
 */
#include "rgr.h"
#include <math.h>

#include <stdio.h>

/* Vyletajut bukvy "И", "В", "Т", "2", "2", "4", 
 * "А", "С", "О", "И", "У" */

struct sliding_letter {
	char letter_string[2];
	POINT end;
};

#define N_LETTERS 12

#define LR_MARGIN 60
#define TB_MARGIN 30

struct sliding_string {
	char s[20];
	LONG y;
};

static const struct sliding_string strings[] = {
	{"ИВТ—224", TB_MARGIN},
	{"АСОИУ", -TB_MARGIN},
	{"", 0},
};

#define N_STEPS 64

static POINT interpolate(POINT a, POINT b, int n);
static void draw_letter(HDC hDC, int cur_str, int cur_ltr, RECT size,
		 POINT origin, int step, struct sliding_string *paStrings);
static void lissajous(HDC hDC, RECT size, int t, double r);

static void copy_strings(struct sliding_string **dest,
		const struct sliding_string *src);

void splash(HANDLE hConsole, HWND hWindow)
{
	HDC hDC, hDC_offscreen;
	RECT size;
	HBRUSH bg;
	HPEN pen, null_pen;
	HFONT font;
	HBITMAP hBMP, hBMP_old;
	int str = 0, ltr = 0, step = 0, i = 0;
	int letter_height;
	int ret;
	double lissajous_radius;
	POINT origin;
	struct sliding_string *paStrings = NULL;

	copy_strings(&paStrings, strings);

	/* Poluchenije rozmerov okna i vychislenije rozmerov bukv
	 * i figur Lissazhu */
	GetClientRect(hWindow, &size);
	origin.x = size.right / 2;
	origin.y = size.bottom;
	letter_height = size.bottom / 10;
	if (letter_height > (size.right - LR_MARGIN * 2 - 30) / 10)
		letter_height = (size.right - LR_MARGIN * 2 - 30) / 10;
	lissajous_radius = (size.bottom - (TB_MARGIN + letter_height)
			    - TB_MARGIN) / 2.0 - 50;

	/* Podstrojka polozhenija strok */
	for (str = 0; paStrings[str].s[0]; str++) {
		paStrings[str].y = ((paStrings[str].y % size.bottom)
				  + size.bottom) % size.bottom;

		if (paStrings[str].y > size.bottom / 2)
			paStrings[str].y -= letter_height;
	}

	hDC = GetDC(hWindow);
	/* Sozdanije vneekrannogo Konteksta ustrojstva */
	hDC_offscreen = CreateCompatibleDC(NULL);
	hBMP = CreateCompatibleBitmap(hDC, size.right, size.bottom);
	hBMP_old = (HBITMAP) SelectObject(hDC_offscreen, hBMP);

	/* Inicializacija objektov i zariadka ix v hDC_offscreen */
	/** shrift */
	font = CreateFont(letter_height, 0, 0, 0,
			  700, FALSE, FALSE, FALSE,
			  RUSSIAN_CHARSET,
			  OUT_DEFAULT_PRECIS,
			  CLIP_DEFAULT_PRECIS,
			  NONANTIALIASED_QUALITY,
			  DEFAULT_PITCH, "Times New Roman");
	SelectObject(hDC_offscreen, font);

	/** belaja kistj */
	bg = CreateSolidBrush(RGB(255, 255, 255));
	SelectObject(hDC_offscreen, bg);

	/* Vylet bukv */
	ret = 1;
	for (str = 0; paStrings[str].s[0]; str++) {
		for (ltr = 0; ltr < strlen(paStrings[str].s); ltr++) {
			for (step = 0; step < N_STEPS; step++) {
				draw_letter(hDC_offscreen, str, ltr,
					    size, origin, step, paStrings);
				BitBlt(hDC, 0, 0, size.right, size.bottom,
				       hDC_offscreen, 0, 0, SRCCOPY);
				Sleep(10);
				if (_kbhit() && _getch() == 27) {
					ret = 0;
					break;
				}
			}
			if (!ret)
				break;
		}
		if (!ret)
			break;
	}
	if (!ret)
		goto graph_end;

	/* Figury Lissazhu */
	/* sineje pero 1 pikselj */
	pen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
	null_pen = SelectObject(hDC_offscreen, pen);
	while (ret) {
		i++;
		SelectObject(hDC_offscreen, null_pen);
		draw_letter(hDC_offscreen, str - 1, ltr - 1, size, origin,
			    N_STEPS - 1, paStrings);
		SelectObject(hDC_offscreen, pen);
		lissajous(hDC_offscreen, size, i, lissajous_radius);
		BitBlt(hDC, 0, 0, size.right, size.bottom,
		       hDC_offscreen, 0, 0, SRCCOPY);
		Sleep(10);
		if (_kbhit() && _getch() == 27) {
			ret = 0;
			break;
		}
	}

	SelectObject(hDC_offscreen, null_pen);
	DeleteObject(pen);
 graph_end:
	DeleteObject(font);
	DeleteObject(bg);
	SelectObject(hDC_offscreen, hBMP_old);
	DeleteObject(hBMP);
	DeleteDC(hDC_offscreen);
	ReleaseDC(hWindow, hDC);
	free(paStrings);
}

static POINT interpolate(POINT a, POINT b, int n)
{
	POINT result;

	result.x = a.x + (b.x - a.x) * (-cos(M_PI / N_STEPS * n) + 1) / 2;
	result.y = a.y + (b.y - a.y) * (-cos(M_PI / N_STEPS * n) + 1) / 2;

	return result;
}

static void draw_letter(HDC hDC, int cur_str, int cur_ltr, RECT size,
		 POINT origin, int step, struct sliding_string *paStrings)
{
	int str, ltr;
	double dx;
	POINT current, end;
	char buf[2] = { 0 };

	current = origin;

	Rectangle(hDC, 0, 0, size.right, size.bottom);
	SetBkMode(hDC, TRANSPARENT);
	SetTextAlign(hDC, TA_CENTER | TA_TOP);
	SetTextColor(hDC, RGB(0, 0, 0));

	for (str = 0; str < cur_str; str++) {
		dx = (double)(size.right - LR_MARGIN * 2)
		    / (strlen(paStrings[str].s) - 1);
		for (ltr = 0; ltr < strlen(paStrings[str].s); ltr++) {
			buf[0] = paStrings[str].s[ltr];
			TextOut(hDC, LR_MARGIN + ltr * dx,
				paStrings[str].y, buf, 1);
		}
	}

	dx = (double)(size.right -
		      LR_MARGIN * 2) / (strlen(paStrings[cur_str].s) - 1);
	for (ltr = 0; ltr < cur_ltr; ltr++) {
		buf[0] = paStrings[cur_str].s[ltr];
		TextOut(hDC, LR_MARGIN + ltr * dx, 
				paStrings[cur_str].y, buf, 1);
	}
	end.y = paStrings[cur_str].y;
	end.x = LR_MARGIN + cur_ltr * dx;
	current = interpolate(origin, end, step);

	buf[0] = paStrings[cur_str].s[cur_ltr];
	TextOut(hDC, current.x, current.y, buf, 1);
}

static void lissajous(HDC hDC, RECT size, int t, double r)
{
	POINT center;
	double x, y, dph;
	int i;

	center.x = size.right / 2;
	center.y = size.bottom / 2;

	dph = (t % 360) * M_PI / 180;
	i = 0;
	x = r * sin(dph);
	y = 0;
	for (i = 0; i <= 360; i++) {
		MoveToEx(hDC, center.x + x, center.y - y, NULL);
		x = r * sin(i * M_PI / 180 + dph);
		y = r * sin(i * M_PI / 180);
		LineTo(hDC, center.x + x, center.y - y);
	}
}

static void copy_strings(struct sliding_string **dest,
		const struct sliding_string *src)
{
	struct sliding_string *strings_copy = NULL;
	int strs;

	for (strs = 0; src[strs].s[0]; strs++);

	strings_copy = calloc(sizeof(struct sliding_string), strs+1);
	for (strs = 0; src[strs].s[0]; strs++) {
		strncpy(strings_copy[strs].s, src[strs].s, 20);
		strings_copy[strs].y = src[strs].y;
	}

	*dest = strings_copy;
}
