/**
 * Rozchotno-graficheskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * tabfn.c
 * Tabulacija funkcij:
 *
 * fn1(x) = 2**x * arctg(x) - root(5, x+1)
 * fn2(x) = exp(a*x**2)
 *
 * Maksimaljnoje i minimaljnoje znacenija - cvetom:
 *     maks. - krasnym
 *     min.  - sinim
 *
 * Во избежание искажений текста все основные комментарии оставлены
 * латиницей.
 */

#include "rgr.h"
#include <math.h>

#define a 3.0
#define b 4.0
#define c 16
#define dx ((b - a) / (c - 1))

static double infinity()
{
	union {
		unsigned long long l;
		double f;
	} x;

	x.l = 0x7fe0000000000000ull;
	return x.f;
}

static double fn1(double x)
{
	return pow(2, x) * atan(x) - root(5, x + 1);
}

static double fn2(double x)
{
	return exp(a * x * x);
}

static void tabulate(double[2][c], double[2], double[2]);
static void print_table(HANDLE, double[2][c], double[2], double[2]);
static void plot_function(HWND, double[c], double, double, char *);

void tabulation(HANDLE hConsole, HWND hWindow)
{
	double x = a;
	double results[2][c];
	double min_result[2] = { infinity(), infinity() };
	double max_result[2] = { -infinity(), -infinity() };
	int i;

	puts("f1(x) = 2**x * arctg(x) - root(5, x + 1)");
	puts("f2(x) = exp(a*x**2)");
	printf("a = %lf, b = %lf\n", a, b);
	puts("");

	tabulate(results, min_result, max_result);
	print_table(hConsole, results, min_result, max_result);
	_getch();
}

void graph(HANDLE hConsole, HWND hWindow)
{
	double results[2][c];
	double min_result[2] = { infinity(), infinity() };
	double max_result[2] = { -infinity(), -infinity() };

	tabulate(results, min_result, max_result);

	plot_function(hWindow, results[0], min_result[0], max_result[0],
		      "y = 2**x*arctg(x) - root(5, x+1)");
	_getch();
	plot_function(hWindow, results[1], min_result[1], max_result[1],
		      "y = exp(a*x**2)");
	_getch();
}

static void tabulate(double results[2][c], double min_result[2],
		     double max_result[2])
{
	double x;
	int i;

	min_result[0] = min_result[1] = infinity();
	max_result[0] = max_result[1] = -infinity();

	for (x = a, i = 0; x <= (b + dx / 2); x += dx, i++) {
		results[0][i] = fn1(x);
		results[1][i] = fn2(x);

		if (min_result[0] > results[0][i])
			min_result[0] = results[0][i];

		if (max_result[0] < results[0][i])
			max_result[0] = results[0][i];

		if (min_result[1] > results[1][i])
			min_result[1] = results[1][i];

		if (max_result[1] < results[1][i])
			max_result[1] = results[1][i];
	}
}

static void print_table(HANDLE hConsole, double results[2][c],
			double min_result[2], double max_result[2])
{
	double x = a;
	int i;

	puts("|   i|           x|          f1"
	     "|                                  f2|");
	puts("|----|------------|------------"
	     "|------------------------------------|");
	for (x = a, i = 0; i < c; i++, x += dx) {
		printf("|%4d", i + 1);
		printf("|%12lf", x);

		putchar('|');
		if (results[0][i] == min_result[0])
			SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE |
						FOREGROUND_INTENSITY);
		else if (results[0][i] == max_result[0])
			SetConsoleTextAttribute(hConsole, FOREGROUND_RED |
						FOREGROUND_INTENSITY);
		printf("%12lf", results[0][i]);
		SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE |
					FOREGROUND_GREEN | FOREGROUND_RED);

		putchar('|');

		if (results[1][i] == min_result[1])
			SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE |
						FOREGROUND_INTENSITY);
		else if (results[1][i] == max_result[1])
			SetConsoleTextAttribute(hConsole, FOREGROUND_RED |
						FOREGROUND_INTENSITY);
		printf("%36lf", results[1][i]);
		SetConsoleTextAttribute(hConsole, FOREGROUND_BLUE |
					FOREGROUND_GREEN | FOREGROUND_RED);

		puts("|");
	}
}

static POINT get_point(POINT origin,
		       double scale_x, double scale_y, double ix, double iy)
{
	POINT result = { 0 };

	result = origin;

	result.x += ix * scale_x;
	result.y -= iy * scale_y;

	return result;
}

static void plot_function(HWND hWindow, double results[c],
			  double fn_min, double fn_max, char *caption)
{
	HDC hDC;
	HBRUSH brush;
	HPEN pen, black;
	RECT size, coord_system;
	POINT origin, p1, p2;
	TEXTMETRIC metrics;
	double scale_x, scale_y;
	double left_x, right_x;
	double top_y, bottom_y;
	double current_x = a, current_y;
	int i;
	char buf[80];

	GetClientRect(hWindow, &size);
	hDC = GetDC(hWindow);

	GetTextMetrics(hDC, &metrics);

	brush = CreateSolidBrush(RGB(255, 255, 255));
	SelectObject(hDC, brush);

	Rectangle(hDC, 0, 0, size.right, size.bottom);

	/* Nastrojka sistemy koordinat */
	coord_system.left = 80;
	coord_system.top = 40;
	coord_system.right = size.right - 20;
	coord_system.bottom = size.bottom - 20;

	/* Vychislenije tochki nachala koordinat */
	left_x = min(0, a - 0.5);
	right_x = max(0, b + 0.5);

	top_y = max(0, fn_max);
	bottom_y = min(0, fn_min);

	scale_x = (coord_system.right - coord_system.left) / (right_x - left_x);
	scale_y = (coord_system.bottom - coord_system.top) / (top_y - bottom_y);

	origin.x = coord_system.left - left_x * scale_x;
	origin.y = coord_system.bottom + bottom_y * scale_y;

	/* Risovanije koordinatnyx osej */
	black = CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
	SelectObject(hDC, black);

	MoveToEx(hDC, 0, origin.y, NULL);
	LineTo(hDC, size.right, origin.y);
	MoveToEx(hDC, origin.x, 0, NULL);
	LineTo(hDC, origin.x, size.bottom);

	SetBkMode(hDC, TRANSPARENT);

	SetTextAlign(hDC, TA_RIGHT | TA_TOP);
	TextOut(hDC, origin.x, origin.y, "O", 1);

	SetTextAlign(hDC, TA_LEFT | TA_BOTTOM);
	TextOut(hDC, origin.x + 5, coord_system.top, "y", 1);
	TextOut(hDC, coord_system.right, origin.y - 5, "x", 1);

	DeleteObject(black);

	/* Risovanije setki */
	black = CreatePen(PS_SOLID, 1, RGB(127, 127, 127));
	SelectObject(hDC, black);

	SetTextAlign(hDC, TA_CENTER | TA_TOP);
	for (current_x = 1; current_x <= b + dx / 2; current_x += 1) {
		p1 = get_point(origin, scale_x, scale_y, current_x, 0);
		MoveToEx(hDC, p1.x, p1.y, NULL);
		LineTo(hDC, p1.x, 0);
		LineTo(hDC, p1.x, coord_system.bottom);

		sprintf(buf, "%g", current_x);
		TextOut(hDC, p1.x, p1.y, buf, strlen(buf));
	}
	/*
	   for (current_x = -1; current_x >= a - dx/2; current_x -= 1) {
	   p1 = get_point(origin, scale_x, scale_y, current_x, 0);
	   MoveToEx(hDC, p1.x, p1.y, NULL);
	   LineTo(hDC, p1.x, 0);
	   LineTo(hDC, p1.x, coord_system.bottom);

	   sprintf(buf, "%g", current_x);
	   TextOut(hDC, p1.x, p1.y, buf, strlen(buf));
	   }
	 */

	SetTextAlign(hDC, TA_RIGHT | TA_BOTTOM);
	for (current_y = fn_max / 16; current_y <= fn_max * 33 / 32;
	     current_y += fn_max / 16) {
		p1 = get_point(origin, scale_x, scale_y, 0, current_y);
		MoveToEx(hDC, p1.x, p1.y, NULL);
		LineTo(hDC, coord_system.left, p1.y);
		LineTo(hDC, size.right, p1.y);

		sprintf(buf, "%.2g", current_y);
		TextOut(hDC, p1.x, p1.y + metrics.tmHeight / 2, buf,
			strlen(buf));
	}

	DeleteObject(black);

	pen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
	SelectObject(hDC, pen);

	p1 = get_point(origin, scale_x, scale_y, a, results[0]);
	for (i = 1, current_x = a + dx; i < c; i++, current_x += dx) {
		MoveToEx(hDC, p1.x, p1.y, NULL);
		p2 = get_point(origin, scale_x, scale_y, current_x, results[i]);
		LineTo(hDC, p2.x, p2.y);

		p1 = p2;
	}

	/* Podpisj funkciji */
	SetTextAlign(hDC, TA_CENTER | TA_TOP);
	TextOut(hDC,
		coord_system.left + (coord_system.right -
				     coord_system.left) / 2, coord_system.top,
		caption, strlen(caption));

}
