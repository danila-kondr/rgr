/**
 * Rozchotno-graficheskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * author.c
 *
 * Vyvod informaciji ob avtore.
 */

#include "rgr.h"

static char *author_info_strings[] = {
	"Автор: Кондратенко Данила Александрович",
	"студент группы ИВТ-224 (кафедра АСОИУ)",
	"2023",
};

void author_info(HANDLE hConsole, HWND hWindow)
{
	HDC hDC;
	RECT size;
	HPEN pen;
	HBRUSH brush, oldBrush;
	TEXTMETRIC metrics;
	LONG i, x, y;

	GetClientRect(hWindow, &size);

	hDC = GetDC(hWindow);
	GetTextMetrics(hDC, &metrics);

	brush = CreateSolidBrush(RGB(255, 0, 0));
	oldBrush = SelectObject(hDC, brush);

	Rectangle(hDC, 0, 0, size.right, size.bottom);

	SetTextAlign(hDC, TA_CENTER);
	SetBkColor(hDC, RGB(255, 0, 0));
	SetTextColor(hDC, RGB(255, 255, 255));

	x = size.right / 2;
	y = (size.bottom -
	     ARRAYSIZE(author_info_strings) * metrics.tmHeight) / 2;
	for (i = 0; i < ARRAYSIZE(author_info_strings); i++) {
		TextOutA(hDC, x, y,
			 author_info_strings[i],
			 strlen(author_info_strings[i]));
		y += metrics.tmHeight;
	}

	SelectObject(hDC, oldBrush);
	ReleaseDC(hWindow, hDC);
	_getch();
}
