/**
 * Rozchotno-graficheskaja rabota.
 *
 * rgr.h
 *
 * Zagolovocnyj fajl, kotoryj objediniajet vojedino vse casti RGR.
 */

#ifndef _RGR_H_
#define _RGR_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <conio.h>
#include <windows.h>

#ifndef max
#define max(a, b) ( ((a) > (b)) ? (a) : (b) )
#endif

#ifndef min
#define min(a, b) ( ((a) < (b)) ? (a) : (b) )
#endif

void cls(HANDLE hConsole);
void clw(HWND hWindow);

double root(double power, double x);

enum MenuItem {
	MENU_ITEM_TABLE = 0,
	MENU_ITEM_GRAPH,
	MENU_ITEM_EQUATION,
	MENU_ITEM_INTEGRAL,
	MENU_ITEM_SPLASH,
	MENU_ITEM_INFO,

	N_MENU_ITEMS
};

int menu(HANDLE hConsole, HWND hWindow);

void tabulation(HANDLE hConsole, HWND hWindow);
void graph(HANDLE hConsole, HWND hWindow);
void equation(HANDLE hConsole, HWND hWindow);
void integral(HANDLE hConsole, HWND hWindow);
void splash(HANDLE hConsole, HWND hWindow);
void author_info(HANDLE hConsole, HWND hWindow);

#endif
