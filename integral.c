/**
 * Rozchotno-graficheskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * integral.c
 *
 * Vychislenije integrala dvumia metodami: metodom trapecij i metodom
 * preamougoljnikov.
 *
 * Integral:
 *     int from a to b (lg(x+1)/(1+x**2)) dx (variant 9)
 */
#include "rgr.h"
#include <math.h>

#define a 0.0
#define b 2.0
#define n 200

static double fn(double x)
{
	return log10(x + 1) / (1 + pow(x, 2));
}

static void integral_trapezium(HANDLE hConsole, HWND hWindow)
{
	double x, y, dx, k = 0;
	int i;

	puts("Вычисление определённого интеграла");
	puts("    k = int from a to b (lg(x+1)/(1+x**2)) dx");
	puts("методом трапеций");

	dx = (b - a) / n;
	printf("a=%lg, b=%lg, n=%d, dx=%lg\n", a, b, n, dx);

	for (x = a, i = 0; i < n; i++, x += dx) {
		k += (fn(x + dx) + fn(x)) / 2 * dx;
	}

	printf("k=%lg\n", k);
}

static void integral_rectangles(HANDLE hConsole, HWND hWindow)
{
	double x, y, dx, k = 0;
	int i;

	puts("Вычисление определённого интеграла");
	puts("    k = int from a to b (lg(x+1)/(1+x**2)) dx");
	puts("методом прямоугольников");

	dx = (b - a) / n;
	printf("a=%lg, b=%lg, n=%d, dx=%lg\n", a, b, n, dx);

	for (i = 0; i < n; i++) {
		x = a + ((double)i - 0.5) * dx;
		k += fn(x)*dx;
	}

	printf("k=%lg\n", k);
}


void integral(HANDLE hConsole, HWND hWindow)
{
	integral_trapezium(hConsole, hWindow);
	puts("");
	integral_rectangles(hConsole, hWindow);

	_getch();
}
