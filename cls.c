/**
 * Rozchotno-graficheskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * console.c
 *
 * Funkcija dlia ochistki konsoli.
 */

#include "rgr.h"

/* https://learn.microsoft.com/en-us/windows/console/clearing-the-screen */
void cls(HANDLE hConsole)
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	SMALL_RECT scroll_rect;
	COORD scroll_target;
	CHAR_INFO fill;

	if (!GetConsoleScreenBufferInfo(hConsole, &csbi))
		return;

	scroll_rect.Left = 0;
	scroll_rect.Top = 0;
	scroll_rect.Right = csbi.dwSize.X;
	scroll_rect.Bottom = csbi.dwSize.Y;

	scroll_target.X = 0;
	scroll_target.Y = (SHORT) (0 - csbi.dwSize.Y);

	fill.Char.UnicodeChar = 0x20;	/* probel */
	fill.Attributes = csbi.wAttributes;

	ScrollConsoleScreenBuffer(hConsole,
				  &scroll_rect, NULL, scroll_target, &fill);

	csbi.dwCursorPosition.X = 0;
	csbi.dwCursorPosition.Y = 0;

	SetConsoleCursorPosition(hConsole, csbi.dwCursorPosition);
}

void clw(HWND hWindow)
{
	RECT size;

	GetClientRect(hWindow, &size);
	InvalidateRect(hWindow, &size, TRUE);
}
