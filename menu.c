/**
 * Rozczotno-graficzeskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * menu.c
 *
 * Procedury meniu.
 */

#include "rgr.h"

static char *menu_items[] = {
	"1) табуляция функции",
	"2) график функций",
	"3) решение уравнения",
	"4) вычисление опред. интеграла",
	"5) графическая заставка",
	"6) информация об авторе",
};

static int print_menu_elements(int pos)
{
	int i;

	puts("            Расчётно-графическая работа ст. гр. ИВТ-224 Кондратенко");
	puts("                 Выберите один из пунктов меню и нажмите Enter");
	puts("                             Для выхода нажмите ESC");

	for (i = 0; i < N_MENU_ITEMS; i++) {
		printf("\n%c %s\n", (i == pos) ? '>' : ' ', menu_items[i]);
	}
}

int menu(HANDLE hConsole, HWND hWindow)
{
	int result = 0;
	int k1, k2;

	while (TRUE) {
		cls(hConsole);
		print_menu_elements(result);
		k1 = _getch();

		if (k1 == 0x00 || k1 == 0xE0) {
			k2 = _getch();
			switch (k2) {
			case 72:
				result--;
				if (result < 0)
					result = 0;
				break;
			case 80:
				result++;
				if (result >= N_MENU_ITEMS)
					result = N_MENU_ITEMS - 1;
				break;
			}
		} else if (k1 == 27) {	/* if ESC */
			return N_MENU_ITEMS;	/* exit */
		} else if (k1 == 13) {	/* if Enter */
			break;	/* load */
		}
	}

	return result;
}
