/**
 * Rozchotno-graficheskaja rabota.
 * Programma prednaznachena dlia OS Windows i izpoljzujet CP1251.
 *
 * root.c
 *
 * Vychislenije kornia proizvoljnoj stepeni.
 */

#include "rgr.h"
#include <math.h>

double root(double power, double x)
{
	double sign = (x > 0) ? 1 : -1;

	if (fmod(power, 2) == 0) {
		return pow(x, 1 / power);
	} else {
		return pow(fabs(x), 1 / power) * sign;
	}
}

